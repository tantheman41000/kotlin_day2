package com.example.demo.controller

import com.example.demo.sevice.ShoppingCartService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController{
    @Autowired
    lateinit var  shoppingCartService: ShoppingCartService
    @GetMapping("/shoppingcart")
    fun getAllShoppingCart() : ResponseEntity<Any>{
        val shoppingCart = shoppingCartService.getShoppingCart()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCart(shoppingCart))
    }
}