package com.example.demo.controller

import com.example.demo.sevice.CustomerService
import com.example.demo.sevice.ProductService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class CustomerController{
    @Autowired
    lateinit var  customerService: CustomerService
    @GetMapping("/customer")
    fun getAllCustomer() : ResponseEntity<Any>{
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomer(customerService.getCustomers()))
    }
}