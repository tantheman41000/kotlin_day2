//package com.example.demo.controller
//
//import com.example.demo.entity.App
//import com.example.demo.entity.Person
//import com.example.demo.entity.Product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//import javax.xml.ws.Response
//
//@RestController
//class HelloWorldController{
//    @GetMapping("/helloWorld")
//    fun getHelloWorld() : String{
//        return "HelloWorld"
//    }
//
//
//    @GetMapping("/getAppName")
//    fun getAppName() :ResponseEntity<Any>{
//        var app01 = App("assessment")
//        return ResponseEntity.ok(app01)
//    }
//
//    @GetMapping("/product")
//    fun getProduct() :ResponseEntity<Any>{
//        var product01 = Product("iPhone","A new telephone",28000.00,5)
//        return ResponseEntity.ok(product01)
//    }
//
//    @PostMapping("/totalPrice")
//    fun getTotalPrice(@RequestBody products: Array<Product>) :ResponseEntity<Any>{
//        var total = 0
//        for (i in products){
////            total = total + i.price
//       }
//        return ResponseEntity.ok(total)
//    }
//
//    @PostMapping ("/avaliableProduct")
//    fun getAvaliable(@RequestBody products: Array<Product>) : ResponseEntity<Any>{
//        var output = mutableListOf<Product>()
//        for (p in products){
//            if (p.quantity > 0){
//                output.add(p)
//            }
//        }
//        return ResponseEntity.ok(output)
//    }
//
//
//
//
//
//
//    @GetMapping("/person")
//    fun getPerson() :ResponseEntity<Any>{
//        var person01 = Person("somsal","sudjapaiboon",55)
//        var person02 = Person("prayuth","Chan",62)
//        var person03 = Person("lung","poom",65)
//        var persons = listOf<Person>(person01,person02,person03)
//        return ResponseEntity.ok(persons)
//    }
//
//    @GetMapping("/myPersons")
//    fun getMyPersons(): ResponseEntity<Any>{
//        var person01 = Person("a","b",55)
//        var person02 = Person("aa","bb",62)
//        var person03 = Person("c","cc",65)
//        var persons = listOf<Person>(person01,person02,person03)
//        return ResponseEntity.ok(persons)
//    }
//    @GetMapping("/myPerson")
//    fun getMyPerson(): ResponseEntity<Any>{
//        var person01 = Person("a","b",55)
//        return ResponseEntity.ok(person01)
//    }
//
//    @GetMapping("/params")
//    fun getParams(@RequestParam("name") name:String,@RequestParam("surname") surname:String)
//    :ResponseEntity<Any> {
//        return ResponseEntity.ok("$name $surname")
//    }
//
//    @GetMapping("/product/{name}")
//    fun getPathproduct(
//            @PathVariable ("name") name:String
//    ): ResponseEntity<Any>{
//        if (name == "iPhone"){
//            var product = Product("iPhone","A new telephone",28000,5)
//            return ResponseEntity.ok(product)
//        }else{
//            return ResponseEntity.notFound().build()
//        }
//
//    }
//
//    @GetMapping("/params/{name}/{surname}/{age}")
//    fun getPathParam (
//            @PathVariable("name") name:String,
//            @PathVariable ("surname") surname:String,
//            @PathVariable ("age") age:Int
//    ): ResponseEntity<Any>{
//        var person =Person(name,surname,age)
//        return ResponseEntity.ok(person)
//    }
//
//    @PostMapping ("/echo")
//    fun echo(@RequestBody person: Person) : ResponseEntity<Any>{
//        return ResponseEntity.ok(person)
//    }
//
//    @PostMapping ("/setZeroQuantity")
//    fun setZeroQuantity(@RequestBody product: Product) :ResponseEntity<Any>{
//        var  newproduct = Product(product.name,product.description,product.price,0)
//        return ResponseEntity.ok(newproduct)
//
//    }
//
//
//
//
//}