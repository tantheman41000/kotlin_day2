package com.example.demo.controller

import com.example.demo.sevice.ProductService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ProductController{
    @Autowired
    lateinit var productService: ProductService
    @GetMapping("/product")
    fun getAllproduct() : ResponseEntity<Any> {
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))
    }
}
