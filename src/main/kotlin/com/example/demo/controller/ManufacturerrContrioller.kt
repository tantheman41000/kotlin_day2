package com.example.demo.controller

import com.example.demo.sevice.ManufacturerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ManufacturerrContrioller {
    @Autowired
    lateinit var  manufacturerService: ManufacturerService
    @GetMapping("/manufacturer")
    fun getAllfacturer(): ResponseEntity<Any>{
        return ResponseEntity.ok(manufacturerService.getManufacturers())
    }

}