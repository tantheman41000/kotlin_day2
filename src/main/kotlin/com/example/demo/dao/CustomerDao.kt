package com.example.demo.dao

import com.example.demo.entity.Customer

interface  CustomerDao{
    fun getCustomers() : List<Customer>
}