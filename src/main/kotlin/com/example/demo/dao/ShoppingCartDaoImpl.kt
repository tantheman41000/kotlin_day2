package com.example.demo.dao

import com.example.demo.entity.ShoppingCart
import com.example.demo.entity.ShoppingCartStatus
import com.example.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("cart")
@Repository
class ShoppingCartDaoImpl: ShoppingCartDao{
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    override fun getShoppingCart() : List<ShoppingCart>{
        return  mutableListOf(ShoppingCart(ShoppingCartStatus.SENT),
                ShoppingCart(ShoppingCartStatus.CONFIRM),
                ShoppingCart(ShoppingCartStatus.PAID),
                ShoppingCart(ShoppingCartStatus.RECEIVED),
                ShoppingCart(ShoppingCartStatus.WAIT)
        )
    }
}