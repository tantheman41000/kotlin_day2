package com.example.demo.dao

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("cus")
@Repository
class CustomerDaoImpl: CustomerDao{

    override fun getCustomers() : List<Customer>{
        return  listOf()
//        return mutableListOf(Customer("Lung","pm@go.th", UserStatus.ACTIVE),
//                Customer("ชัชชาติ","chut@taopoon.com",UserStatus.ACTIVE),
//                Customer("ธนาธร","thanathorn@life.com",UserStatus.PENDING))
    }
}