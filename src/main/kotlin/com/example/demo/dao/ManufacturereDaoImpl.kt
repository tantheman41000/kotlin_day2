package com.example.demo.dao

import com.example.demo.entity.Manufacturer
import com.example.demo.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ManufacturereDaoImpl : ManufacturerDao{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    override fun getManufacturers() : List<Manufacturer> {
        return mutableListOf(Manufacturer("Apple", "053123456")
                , Manufacturer("Samsung", "5555666777878"),
                Manufacturer("CAMT", "00000000000000"))
    }
}

