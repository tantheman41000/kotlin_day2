package com.example.demo.dao

import com.example.demo.entity.Product
import com.example.demo.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ProductDaoDBImpl: ProductDao{
    @Autowired
    lateinit var productRepository: ProductRepository
    override fun getproducts(): List<Product> {
        return productRepository.findAll() as List<Product>
    }
}