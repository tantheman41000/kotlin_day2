package com.example.demo.dao

import com.example.demo.entity.ShoppingCart
import com.example.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDBDaoImpl: ShoppingCartDao{
    @Autowired
    lateinit var ShoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCart(): List<ShoppingCart> {
        return ShoppingCartRepository.findAll() as List<ShoppingCart>
    }
}