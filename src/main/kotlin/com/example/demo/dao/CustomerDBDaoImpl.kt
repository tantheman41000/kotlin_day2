package com.example.demo.dao

import com.example.demo.entity.Customer
import com.example.demo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDBDaoImpl: CustomerDao{
    @Autowired
    lateinit var customerRepository: CustomerRepository
    override fun getCustomers(): List<Customer>{
        return customerRepository.findAll() as List<Customer>
    }
}