package com.example.demo.dao

import com.example.demo.entity.Manufacturer

interface  ManufacturerDao{
    fun getManufacturers() : List<Manufacturer>
}