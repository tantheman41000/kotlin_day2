package com.example.demo.dao

import com.example.demo.entity.Product

interface  ProductDao{
    fun getproducts() : List<Product>
}