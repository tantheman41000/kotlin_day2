package com.example.demo.dao

import com.example.demo.entity.ShoppingCart

interface ShoppingCartDao{
    fun getShoppingCart(): List<ShoppingCart>
}