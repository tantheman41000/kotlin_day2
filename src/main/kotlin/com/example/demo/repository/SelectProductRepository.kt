package com.example.demo.repository

import com.example.demo.entity.SelectProduct
import org.springframework.data.repository.CrudRepository

interface SelectProductRepository : CrudRepository<SelectProduct,Long>