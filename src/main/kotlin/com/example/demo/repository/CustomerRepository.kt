package com.example.demo.repository

import com.example.demo.entity.Customer
import com.example.demo.entity.Product
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer,Long>

