package com.example.demo.repository

import com.example.demo.entity.ShoppingCart
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository: CrudRepository<ShoppingCart,Long>