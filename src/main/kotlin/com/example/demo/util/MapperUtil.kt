package com.example.demo.util

import com.example.demo.entity.*
import com.example.demo.entity.dto.*
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers


@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    @Mappings(
            Mapping(source = "manufacturer", target = "manu")
    )

    fun mapProductDto(product: Product):ProductDto

    fun mapProductDto(products:List<Product>): List<ProductDto>

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto

    fun mapCustomer(customer: Customer) : CustomerDto
    fun mapCustomer(customers: List<Customer>): List<CustomerDto>

    fun mapShoppingCart(shoppingCart: ShoppingCart) : ShoppingCartDto
    fun mapShoppingCart(shoppingCart: List<ShoppingCart>) : List<ShoppingCartDto>

    fun mapSelectProduct(selectProduct: SelectProduct) : SelectProductDto
    fun mapSelectProduct(selectProduct: List<SelectProduct>) : List<SelectProductDto>

//    fun mapAddress(address: Address): AdressDto
//    fun mapAdress(address: List<Address>): AdressDto
}
