package com.example.demo.entity
import com.example.demo.entity.Product
import org.apache.tomcat.jni.Address
import javax.annotation.Generated
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class Manufacturer (var name: String, var telNo: String) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany(mappedBy = "manufacturer")
    var products = mutableListOf<Product>()
}