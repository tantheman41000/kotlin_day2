package com.example.demo.entity

import javax.persistence.*

@Entity
data class ShoppingCart(
        var shoppingCartStatus: ShoppingCartStatus? ) {
    @Id @GeneratedValue
    var id:Long? = null
    @OneToMany
    var selectProduct = mutableListOf<SelectProduct>()
    @OneToOne
    lateinit var customer: Customer
    @OneToOne
    lateinit var address: Address

}