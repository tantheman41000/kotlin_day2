package com.example.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class SelectProduct(var quantity:Int? = null){
    @Id
    @GeneratedValue
    var Id:Long? = null
    @OneToOne
    lateinit var selectProduct : Product
}