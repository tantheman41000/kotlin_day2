package com.example.demo.entity.dto

import com.example.demo.entity.Address
import com.example.demo.entity.UserStatus

data class  CustomerDto(
        var name:String? = null,
        var email: String? = null,
        var userStatus: UserStatus? = null,
        var billingAddress: Address? = null,
        var defaultAddress: Address? = null,
        var shippingAddress: Address? = null
)