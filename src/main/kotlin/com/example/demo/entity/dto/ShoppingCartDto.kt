package com.example.demo.entity.dto

import com.example.demo.entity.Address
import com.example.demo.entity.Customer
import com.example.demo.entity.SelectProduct
import com.example.demo.entity.ShoppingCartStatus

data class ShoppingCartDto(

        var shoppingCartStatus: ShoppingCartStatus? = null,
        var selectProduct: List<SelectProductDto>? = null,
        var customer: Customer? = null,
        var address: Address? = null
)