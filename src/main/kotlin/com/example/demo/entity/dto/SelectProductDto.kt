package com.example.demo.entity.dto

import com.example.demo.entity.Product
import com.example.demo.entity.SelectProduct

data class SelectProductDto(
        var quantity:Int? = null,
        var selectProduct: ProductDto? = null

)
