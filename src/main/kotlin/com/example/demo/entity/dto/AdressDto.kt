package com.example.demo.entity.dto

data class AdressDto(
        var homeAdress: String? = null,
        var subdistict: String? = null,
        var distict: String? = null,
        var province: String? = null,
        var postCode : String? = null
)