package com.example.demo.entity

import com.example.demo.entity.ShoppingCart
import javax.persistence.*


@Entity
data class Address(
        var homeAdress: String? = null,
        var subdistict: String? = null,
        var distict: String? = null,
        var province: String? = null,
        var postCode : String? = null
){
    @Id
    @GeneratedValue
    var id:Long? = null


}