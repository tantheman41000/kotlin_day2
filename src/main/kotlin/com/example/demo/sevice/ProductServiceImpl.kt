package com.example.demo.sevice

import com.example.demo.dao.ProductDao
import com.example.demo.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductServiceImpl: ProductService{
    @Autowired
    lateinit var productDao: ProductDao

    override fun getProducts(): List<Product> {
        return productDao.getproducts()
    }
}
