package com.example.demo.sevice

import com.example.demo.dao.CustomerDao
import com.example.demo.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl: CustomerService{
    @Autowired
    lateinit var  customerDao: CustomerDao
    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}