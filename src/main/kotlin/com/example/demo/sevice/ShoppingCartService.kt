package com.example.demo.sevice

import com.example.demo.entity.ShoppingCart

interface ShoppingCartService{
    fun getShoppingCart() : List<ShoppingCart>
}

