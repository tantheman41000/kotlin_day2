package com.example.demo.sevice

import com.example.demo.dao.ManufacturerDao
import com.example.demo.entity.Manufacturer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufactuererServiceImpl: ManufacturerService{
    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }
}