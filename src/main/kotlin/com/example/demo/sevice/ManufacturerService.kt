package com.example.demo.sevice

import com.example.demo.entity.Manufacturer

interface ManufacturerService {
    fun getManufacturers(): List<Manufacturer>
}
