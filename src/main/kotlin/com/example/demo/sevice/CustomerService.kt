package com.example.demo.sevice

import com.example.demo.entity.Customer

interface CustomerService{
    fun getCustomers() : List<Customer>
}