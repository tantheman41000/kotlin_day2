package com.example.demo.sevice

import com.example.demo.dao.ShoppingCartDao
import com.example.demo.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShoppingCartImpl: ShoppingCartService{
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCart(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCart()
    }
}