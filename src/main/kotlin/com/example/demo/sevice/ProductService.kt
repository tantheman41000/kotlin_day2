package com.example.demo.sevice

import com.example.demo.entity.Product

interface ProductService{
    fun getProducts() : List<Product>
}
