package com.example.demo.config

import com.example.demo.entity.*
import org.omg.PortableInterceptor.ACTIVE
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional
import com.example.demo.entity.Address
import com.example.demo.repository.*

@Component
class ApplicationLoader:ApplicationRunner{
@Autowired
lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var selectProductRepository: SelectProductRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Transactional
    override fun run(args: ApplicationArguments?) {
        var manu1 = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("Samsung","5555666777878"))
        var manu3 = manufacturerRepository.save(Manufacturer("Apple","053123456"))
        var product1 = productRepository.save(Product("CAMT",
                "the best college in CMU",
                0.0,
                1,
                "http://www.camt.cmu.ac.th/th/images/logo.jpg"
                ))
        var product2 = productRepository.save(Product("iPhone","it's an iPhone",0.0,
                5,"www.youtube.com"))
        var product3 = productRepository.save(Product("Prayuth","It's a book",0.0,
                1,"www.google.com"))
        var product4 = productRepository.save(Product("note 9", "it's a phone", 20.00,
                12,"www.samsung.co.th"))
        var selectproduct1 = selectProductRepository.save(SelectProduct(4))
        var selectproduct2 = selectProductRepository.save(SelectProduct(1))

        var selectproduct3 = selectProductRepository.save(SelectProduct(1))
        var selectproduct4 = selectProductRepository.save(SelectProduct(1))
        var selectproduct5 = selectProductRepository.save(SelectProduct(2))

        var shoppingcart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var shoppingcart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))
        var cust1 = customerRepository.save(Customer("Lung","pm@go.th",UserStatus.ACTIVE))
        var cust2 = customerRepository.save(Customer("ชัชชาติ","chut@taopoon.com",UserStatus.ACTIVE))
        var cust3 = customerRepository.save(Customer("ธนาธร","thanathorn@life.com",UserStatus.PENDING))
        var addr1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย",
                "แขวง ดินสอ"," เขตดุสิต","กรุงเทพ ","10123"))
        var addr2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่",
                "ต.สุเทพ","อ.เมือง","จ.เชียงใหม่ ","50200"))
        var addr3 = addressRepository.save(Address("ซักที่บนโลก","ต.สุขสันต์",
                "อ.ในเมือง ", "จ.ขอนแก่น ","12457"))
        manu1.products.add(product1)
        manu2.products.add(product4)
        manu3.products.add(product2)
        manu1.products.add(product3)
        product1.manufacturer = manu1
        product2.manufacturer = manu3
        product3.manufacturer = manu1
        product4.manufacturer = manu2



        cust1.billingAddress = addr1
        cust1.shippingAdress = mutableListOf(addr1)
        cust1.defaultAdress = addr1
        selectproduct1.selectProduct = product2

        selectproduct2.selectProduct = product3
        shoppingcart1.selectProduct.add(selectproduct1)
        shoppingcart1.selectProduct.add(selectproduct2)
        shoppingcart1.customer = cust1
        shoppingcart1.address = addr1




        cust2.billingAddress = addr2
        cust2.shippingAdress = mutableListOf(addr2)
        cust2.defaultAdress = addr2
        selectproduct3.selectProduct = product3
        selectproduct4.selectProduct = product1
        selectproduct5.selectProduct = product4
        shoppingcart2.selectProduct.add(selectproduct3)
        shoppingcart2.selectProduct.add(selectproduct4)
        shoppingcart2.selectProduct.add(selectproduct5)
        shoppingcart2.customer = cust2
        shoppingcart2.address = addr2



        cust3.billingAddress = addr3
        cust3.shippingAdress = mutableListOf(addr3)
        cust3.defaultAdress = addr3



    }
}

